package ut.com.transcendmanagement.jira.plugin;

import org.junit.Test;
import com.transcendmanagement.jira.plugin.MyPluginComponent;
import com.transcendmanagement.jira.plugin.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}